<?php
session_start();
if(isset($_POST["login"])){
     $dsn= "mysql:host=localhost;dbname=imc";
    $user="root";
    $mdp="";
    try{
        $connexion = new PDO($dsn,$user,$mdp); 
        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //récupération du user dont le login est saisi
        $req = "SELECT pass FROM user WHERE login= :l";
        $traitement = $connexion->prepare($req);
        $traitement->bindparam(':l', $_POST["login"]);
        $traitement->execute();
        //parcours du résultat
        $row=$traitement->fetch();
        //vérif du password
        if(password_verify($_POST["pass"],$row['pass'])){
            
            echo "connexion OK - ouvrir session ensuite";
            $_SESSION["login"] = $_POST["login"];
            header("resultats.php");
        }else{
            echo "Erreur de login ou mot de passe";
        }
        
        


        $connexion=NULL;
    }catch(PDOException $e){
        die($dsn."erreur".$e->getMessage());
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Connexion</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='css.css'>
    <script src='main.js'></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>
    <h1>Sondage calcul IMC / tranche d'âge & Régime</h1>
    <form method="POST" action="">
        <div class="form-group">
            <label for="login">Login</label><input type="text" name="login" id="login" class="form-control" required/><br/>
            <label for="pass">Password</label><input type="password" name="pass" id="pass" class="form-control" required/><br/>
            <input type="submit" value="Envoyer">
        </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>
</html>