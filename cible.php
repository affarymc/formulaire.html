<?php
if (preg_match_all("/[\W]/",$p)>1 && preg_match_all("/[0-9]/",$p)>0 && strlen($p)>7){ //si pass ok
    $dsn="mysql:host=localhost;dbname=imc";
    $user="root";
    $mdp="";
 try {
     $connexion = new PDO($dsn,$user,$mdp);
     $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     //insertion dans la table user
     $req="insert into user(login,pass) values (:1, :p)";
     $traitement = $connexion->prepare($req);
     $traitement->bindparam(':1', $_POST["login"]);
     $traitement->bindparam(':p', $_POST["pass"]);
     $traitement->execute();
     //insertion dans la table resultats
     $req = "insert into resultats(dateNaissance, regime, poids, taille) values (:dn,:r,:p,:t)";
     $traitement = $connexion->prepare($req);
     $traitement->bindparam(':dn', $_POST["dateNaissance"]);
     $traitement->bindparam(':r', $_POST["regime"]);
     $traitement->bindparam(':p', $_POST["poids"]);
     $traitement->bindparam(':t', $_POST["taille"]);
     $traitement->execute();
     
     $connexion=NULL;
 }catch(PDOException $e){
     die($dsn."erreur".$e->getMessage());
 }
}
?>
